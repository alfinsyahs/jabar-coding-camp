//Tugas-5-Javascript-Array-Function-Object
//Alfinsyah Suhendra //Vue.js

//Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i=1; i< daftarHewan.length; i++) {
    for (var j = 0; j < i; j++) {
        if (daftarHewan[i] < daftarHewan[j]){
         var x = daftarHewan[i];
         daftarHewan[i] = daftarHewan[j];
         daftarHewan[j] = x;
        }
    }   

}
console.log('Nama Hewan');
for (var k = 0; k < daftarHewan.length; k++) 
console.log(daftarHewan[k]);

//Soal 2
var data = {name : "Alfinsyah" , age: 24 , address: "Cirebon", hobby : "nonton film"}

function introduction(perkenalan) {
    var nama = perkenalan.name
    var umur = perkenalan.age
    var alamat = perkenalan.address
    var hobi = perkenalan.hobby
    return "Nama saya" + " " + nama + "," + " " + "umur saya" + umur + "tahun," + " " +
    "Alamat saya di " + alamat + "," + " " + " dan saya punya hobby yaitu" + " "+ hobi
}
var perkenalan = introduction (data)
console.log(perkenalan)

//Soal 3

function hitung_huruf_vokal(str) {
    var count = str.match(/[aiueo]/gi).length;
    return count;
}

var hitung1 = hitung_huruf_vokal('Alfinsyah'); //3
var hitung2 = hitung_huruf_vokal('Suhendra'); //3
console.log(hitung1,hitung2)

//Soal 4

function hitung(angka) {
    return (angka*2-2)
}

console.log(hitung(0)) //-2
console.log(hitung(1)) //0
console.log(hitung(2)) //2
console.log(hitung(3)) //4
console.log(hitung(4)) //6

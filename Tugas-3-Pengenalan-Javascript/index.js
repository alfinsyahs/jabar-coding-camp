//Alfinsyahs-Vue.js

//soal 1
var pertama= "saya sangat senang hari ini";
var kedua= "belajar javascript itu keren";

var pertama1 = pertama.substr(0,4) + " "; //saya
var pertama2 = pertama.substr(12,6) + " "; //senang
var kedua1 =  kedua.substring(0,8); //belajar
var kedua2 = kedua.substr(8,10); //javascript
var upper = kedua2.toUpperCase(); //JAVASCRIPT


console.log(pertama1+pertama2+kedua1+upper);

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var intPertama = parseInt(kataPertama); //10
var intKedua = parseInt(kataKedua); //2
var intKetiga = parseInt(kataKetiga); //4
var intKeempat = parseInt(kataKeempat); //6

var hitung = (intPertama + (intKedua * intKetiga) + intKeempat);

console.log(hitung);

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0,3); //wah
var kataKedua = kalimat.substring(4,14); //javascript
var kataKetiga = kalimat.substring(15,18); //itu
var kataKeempat = kalimat.substring(19,24); //keren
var kataKelima = kalimat.substring(25,31); //sekali

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//Selesai
//Alhamdulillah
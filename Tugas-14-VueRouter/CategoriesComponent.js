export const CategoriesComponent = {
    data () {
        return {
            categories: [
                {
                    id: 1,
                    title: 'UCL 2005'
                },
                {
                    id: 2,
                    title: 'UCL 2019'
                },
                {
                    id: 3,
                    title: 'EPL 2020'
                }
            ]
        }
    },
    template: `
        <div>
            <h1>Trofi Liverpool</h1>
            <ul>
                <li v-for="category in categories">
                    <router-link :to="'/category/'+category.id">
                        {{ category.title}}
                    </router-link>
                </li>
            </ul>
            <br><br><br><br><br>Halaman 3
        </div>
    `
}
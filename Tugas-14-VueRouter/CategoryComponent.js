export const CategoryComponent = {
    data(){
        return {
            categories: [
                {
                    id: 1,
                    title: 'Juara UCL 2005',
                    description : 'Final melawan AC Milan di babak penalti'
                },
                {
                    id: 2,
                    title: 'Juara UCL 2019',
                    description : 'Final Melawan Real Madrid'

                },
                {
                    id: 3,
                    title: 'Juara EPL 2020',
                    description : 'Klasemen pertama di English Premier League'

                },
                
            ]
        }
    },
    computed: {
        category() {
            return this.categories.filter((category)=>{
                return category.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `<div >
            <h1 class="category">Category :  {{ category.title }}</h1>
            <ul>
                <li v-for="(num, value) of category">
                    {{ num +' : '+ value }} <br>
                    </li>
            </ul>
            <br><br><br><br><br>Halaman 3
        </div>`,
   
}
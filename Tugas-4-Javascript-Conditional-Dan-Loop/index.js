//Tugas-4-Conditional-Loop

//Soal 1

Var Nilai = 90;
if ( Nilai >= 85){
    console.log("Nilai = A");
} else if (Nilai >= 75 && Nilai < 85) {
    console.log("Nilai = B");
} else if (Nilai >= 65 && Nilai < 75) {
    console.log("Nilai = C");
} else if (Nilai >= 55 && Nilai < 65) {
    console.log("Nilai = D")
} else if (Nilai < 55) {
    console.log("Nilai = E")
}





//Soal 2

var tanggal = 3;
var bulan = 1;
var tahun = 1997;

switch (bulan){
    case 1 : var bulan = "Januari"; 
    break;
    case 2 : var bulan = "Februari";
    break;
    case 3 : var bulan = "Maret";
    break;
    case 4 : var bulan = "April";
    break;
    case 5 : var bulan = "Mei";
    break;
    case 6 : var bulan = "Juni";
    break;
    case 7 : var bulan = "Juli";
    break;
    case 8 : var bulan = "Agustus";
    break;
    case 9 : var bulan = "September";
    break;
    case 10 : var bulan = "Oktober";
    break;
    case 11 : var bulan = "November";
    break;
    case 12 : var bulan = "Desember";
}

var tbt = tanggal + " " + bulan + " " + tahun ;

console.log(tbt);




//Soal 3

//untuk n=3
var n="";
    for (i=1; i<=3; i++)
    {   
         for (j=1; j<=i; j++)
         {
          n=n + "#";   
         }
       
         console.log(n + "");
         n="";   
    }

//untuk n=7
var n="";
    for (i=1; i<=7; i++)
    {   
         for (j=1; j<=i; j++)
         {
          n=n + "#";   
         }
       
         console.log(n + "");
         n="";   
    }





    //soal 4

//untuk m=3
const kata = ["Programming","Javascript", "VueJS"];
//array kata
let panjang = kata.length;
let indeks = 0;
let no = 1;
input = 3;

while (no <= input) {
    console.log(no + " - I love" + kata[indeks]);
    indeks += 1;
    no += 1;

    //jika indeks ==  panjang kata, maka kembali ke indeks = 0
    if (indeks == panjang) {
        console.log("=".repeat(no -1));
        indeks = 0;
    }
}

//untuk m=5
const kata = ["Programming","Javascript", "VueJS"];
//array kata
let panjang = kata.length;
let indeks = 0;
let no = 1;
input = 5;

while (no <= input) {
    console.log(no + " - I love" + kata[indeks]);
    indeks += 1;
    no += 1;

    //jika indeks ==  panjang kata, maka kembali ke indeks = 0
    if (indeks == panjang) {
        console.log("=".repeat(no -1));
        indeks = 0;
    }
}

//untuk m=7
const kata = ["Programming","Javascript", "VueJS"];
//array kata
let panjang = kata.length;
let indeks = 0;
let no = 1;
input = 7;

while (no <= input) {
    console.log(no + " - I love" + kata[indeks]);
    indeks += 1;
    no += 1;

    //jika indeks ==  panjang kata, maka kembali ke indeks = 0
    if (indeks == panjang) {
        console.log("=".repeat(no -1));
        indeks = 0;
    }
}

//untuk m=10
const kata = ["Programming","Javascript", "VueJS"];
//array kata
let panjang = kata.length;
let indeks = 0;
let no = 1;
input = 10;

while (no <= input) {
    console.log(no + " - I love" + kata[indeks]);
    indeks += 1;
    no += 1;

    //jika indeks ==  panjang kata, maka kembali ke indeks = 0
    if (indeks == panjang) {
        console.log("=".repeat(no -1));
        indeks = 0;
    }
}
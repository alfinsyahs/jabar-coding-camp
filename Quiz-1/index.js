//Alfinsyah Suhendra
//Quiz-1

//Soal 1

var tanggal = 3
var bulan = 1
var tahun = 2021

function tanggalBesok(tanggal, bulan, tahun){
    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var selectmonth = months[bulan-1];
    var fusion = selectmonth + '/' + tanggal.toString() + '/' +tahun.toString();
    var s = new Date(fusion);
    s.setDate(s.getDate() +1);
    return `${s.getDate()} ${months[s.getMonth()]} ${s.getFullYear()}`
}

var hasil = tanggalBesok(tanggal, bulan, tahun);
console.log(hasil);

//Soal 2

function jumlah(string) {
    var pemisah = string.split(' ').filter((kata) => {if (kata !== ' ')
    return kata;
})
    return pemisah.length;
}
   
var kalimat_1 = "Halo nama saya Alfinsyah Suhendra"
var kalimat_2 = "Saya Alfinsyah Suhendra"
var hasil_1 = jumlah(kalimat_1)
var hasil_2 = jumlah(kalimat_2)

console.log(kalimat_1 + '--> Jumlah_kata(kalimat_1) =' + " " + hasil_1 ) //5
console.log(kalimat_2 + '--> Jumlah_kata(kalimat_2) =' + " " + hasil_2 ) //3